export default [{
  // 列表demo
  "url": "demo/list",
  "data": {
    "data": {
      "count": 10,
      "list|10": [{
        "id|+1": 1,
        "name": "@cname",
        "province":"@province",
        "city":"@city",
        "address":"@county()",
        "zip":"@zip",
        "date":'@date("yyyy-MM-dd")'
      }]
    },
    "errorCode": 0,
    "errorInfo": "成功",
    "success": true
  }
},
// 详情demo
{
  "url": "demo/detail",
  "method": 'get',
  "data": {
    "data": {
      "id|+1": 1,
      "name": "@cname",
      "province":"@province",
      "city":"@city",
      "address":"@county()",
      "zip":"@zip",
      "date":'@date("yyyy-MM-dd")'
    },
    "errorCode": 0,
    "errorInfo": "成功",
    "success": true
  }
}]
