## 项目目录
<!-- 业务全部放在src目录下 -->
.src
├── main.js
├── api           <!-- 接口模块 -->
│   ├── index.js
│   ├── path
│   │   ├── user.js         <!-- 用户模块 -->
│   │   ├── permission.js   <!-- 权限模块 -->
│   │   └── mock.js         <!-- 本地mock模拟接口 -->
│   ├── http.js          <!-- 请求方法 -->
│   ├── path.js          <!-- 统一接口路径入口 -->
│   └── url.js              <!-- dev/pro环境Url -->
├── assets                  <!-- 图片资源,按照目录划分 -->
│   └── logo.png
├── common                <!-- 公用脚本 -->
│   ├── directive           <!-- 公用指令 -->
│   │   └── permission.js         <!-- 权限显示命令 -->
│   ├── mixins              <!-- vuex -->
│   │   ├── vuex            <!-- vuex导入模块 -->
│   │   │   ├── home.js
│   │   │   └── permission.js
│   │   └── created.js      <!-- 导入默认vuex模块 -->
│   ├── socket              <!-- wssocket -->
│   │   ├── event.js            <!-- pubsub事件注册 -->
│   │   ├── message_event.js    <!-- pubsub事件处理 -->
│   │   └── socket.js           <!-- wssocket连接处理 -->
│   ├── styles              <!-- 公用样式 -->
│   │   ├── flex.less       <!-- 公用流式布局样式 -->
│   │   └── var.less        <!-- 公用样式命名 -->
│   └── utils              <!-- 公用工具 -->
│       └── timeUtil.js       <!-- 时间工具 -->
├── components                <!-- 公用组件模块 -->
│   └── HelloWorld              <!-- 组件 -->
│       ├── index.jsx
│       └── index.scss
├── router                  <!-- vue-router -->
│   └── index.js    
├── store                  <!-- vuex具体模块 -->
│   ├── home        
│   ├── permission        
│   ├── template      
│   │   ├── actions.js      
│   │   ├── getters.js     
│   │   ├── mutations.js    
│   │   ├── state.js        
│   │   └── index.js        
│   ├── util.js        
│   └── index.js    
└── views                       <!-- 业务视图层 -->
    ├── home                    <!-- 首页 -->
    │   ├── breadcrumb          <!-- 面包屑标签 -->
    │   │   ├── index.js
    │   │   ├── index.less
    │   │   └── index.vue
    │   ├── header              <!-- 头部 -->
    │   │   ├── index.js
    │   │   ├── index.less
    │   │   └── index.vue
    │   ├── menu                <!-- 左侧菜单栏 -->
    │   │   ├── index.js
    │   │   ├── index.less
    │   │   └── index.vue
    │   ├── index.jsx
    │   ├── index.scss
    │   └── mock.js
    ├── route                   <!-- 全局路由入口 -->
    │   ├── index.js    
    │   └── index.vue
    ├── no-route                <!-- 空默认页 -->
    │   ├── index.js
    │   ├── index.less
    │   └── index.vue
    └── template                <!-- 模板 -->
            ├── index.js
            ├── index.less
            └── index.vue

