module.exports = {
    "root": true,
    "env": {
        "browser": true,
        "es6": true,
        "jest": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:vue/recommended",
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly",
        "module": "readonly",
        "process": "readonly",
        "__dirname": "readonly",
    },
    "parserOptions": {
        parser: 'babel-eslint',
        "ecmaVersion": 2018,
        "sourceType": "module",
    },
    "plugins": [
        "vue",
    ],
    "rules": {
        // 禁用import和require必须出现在顶层作用域的验证规则
        "global-require": 0,
        'no-new': 0,
        'no-shadow': 0,
        'camelcase': 0,
        'no-bitwise': 0,
        'func-names': 0,
        'no-console': 0,
        'no-plusplus': 0,
        'arrow-parens': 0,
        'indent': ['error', 2],
        'comma-dangle': 0,
        'default-case': 0,
        'prefer-template': 0,
        'consistent-return': 0,
        'no-param-reassign': 0,
        'no-nested-ternary': 0,
        'operator-linebreak': 0,
        'import/no-unresolved': 0,
        'object-curly-newline': 0,
        'no-unused-expressions': 0,
        'no-restricted-globals': 0,
        'max-len': ['error', { code: 150 }],
        'prefer-destructuring': ['error', { object: true, array: false }],
        'import/prefer-default-export': 0,
        'import/no-extraneous-dependencies': 0,
        'vue/no-v-html': 0,
        'vue/attributes-order': 0,
        'vue/require-v-for-key': 0,
        'vue/require-default-prop': 0,
        'vue/no-unused-components': 0,
        'vue/singleline-html-element-content-newline': 0,
        'vue/name-property-casing': ['error', 'kebab-case'],
        'vue/component-name-in-template-casing': ['error', 'kebab-case'],
        'vue/html-closing-bracket-newline': 2
    }
};
