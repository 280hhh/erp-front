/**
 * v-permission
 * <div v-permission:add>
 * <div v-permission:edit>
 * <div v-permission:delete>
 */

export default {
  bind(el,bind,vnode){
    // arg[edit:有管理权限才显示]
    let {arg} = bind;
    let {context} = vnode;
    let {path} = context.$route;
    let state = context.permissionStateGet[path];
    if(!state){
      return hiddenElement(el);
    }
    if(arg=='edit'){
      if(state.nature){
        return;
      }
      hiddenElement(el);
    }
    function hiddenElement(el){
      el.style.display = "none";
      // el.style.cursor = "not-allowed";
    }
  },
  install(Vue) {
    Vue.directive('permission', {
      bind:this.bind,
    });
  }
};
