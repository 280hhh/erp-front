const Production = resolve => require.ensure([], () => resolve(require('@/views/production/index.vue')), 'user')

export default {
  path: "/production",
  component: Production,
  children:[],
  meta:{
    name:"生产管理"
  }
}
