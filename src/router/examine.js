const Examine = resolve => require.ensure([], () => resolve(require('@/views/examine/index.vue')), 'user')

export default {
  path: "/examine",
  component: Examine,
  children:[],
  meta:{
    name:"审核页面"
  }
}
