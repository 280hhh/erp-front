const User = resolve => require.ensure([], () => resolve(require('@/views/user/index.vue')), 'user')

export default {
  path: "user",
  component: User,
  children:[],
  meta:{
    name:"用户管理"
  }
}
