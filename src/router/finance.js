const Finance = resolve => require.ensure([], () => resolve(require('@/views/finance/index.vue')), 'user')

export default {
  path: "/finance",
  component: Finance,
  children:[],
  meta:{
    name:"财务管理"
  }
}
