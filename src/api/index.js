import http from './http';
import apiPath from './path';

export {
  http,
  apiPath
}