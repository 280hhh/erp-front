const path = {
  getConfig:"v1/power/get_menu",
  list:"v1/power/get_list",
  getDetail:"v1/power/get_detail",
  upsert:"v1/power/upsert",
  del:"v1/power/del",
}
export default path;
