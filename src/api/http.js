import axios from 'axios';
import BaseUrl from "./url"
import Vue from "vue"
import router from "../router"
import store from "../store"

const jsonParseReg = /^\{.*\}$/

function responseJsonParse(data){
  let keys = Object.keys(data);
  if(!keys.length){
    return data;
  }
  keys.forEach((key)=>{
    let value = data[key];
    if(jsonParseReg.test(value)){
      data[key] = JSON.parse(value)
    }
  })
  return data
}

let httpLock = false;
// contentType为'j'标识 application/json
// contentType为'f'标识 application/x-www-form-urlencoded
// contentType为'm'标识 multipart/form-data

const http = (url, data = {}, method = "POST",contentType='j',config={})=>{
  // 处理本地过期策略
  if(httpLock) return;
  let getStorage = localStorage.getItem("user-loginInfo");
  const localStroage = getStorage && JSON.parse(getStorage)||{};
  let {access_token="",expired_time=0} = localStroage;
  if(expired_time){
    if(expired_time<=new Date().getTime()){
      access_token = "";
      localStorage.setItem('user-loginInfo','')
      httpLock = true;
      setTimeout(()=>{
        window.location.reload()
      },0)
    }else{
      // 过期时间设置为30小时
      expired_time = new Date().getTime() + 30*60*1000;
      localStroage.expired_time =expired_time;
      localStorage.setItem('user-loginInfo',JSON.stringify(localStroage));
    }
  }
  switch (contentType) {
  case "j": axios.defaults.headers.common['Content-Type'] = 'application/json;charset=utf-8';break;
  case 'f':
    axios.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    var urlparams = new URLSearchParams();
    Object.keys(data).map(el=> {
      urlparams.append(el,data[el]);
    })
    data = urlparams;
    break;
  case "m":
    axios.defaults.headers.common['Content-Type'] = 'multipart/form-data;charset=utf-8';
    var formData = new FormData();
    Object.keys(data).map(el=> {
      formData.append(el,data[el]);
    })
    data = formData;
    break;
  default: axios.defaults.headers.common['Content-Type'] = 'application/json;charset=utf-8';break;
  }
  console.log(axios.defaults.headers.common['Content-Type'])
  // 这里加_ 是因为axios库内部占用了AccessToken,会重置掉自定义内容
  axios.defaults.headers['Authorization'] = access_token;
  axios.defaults.withCredentials = true;
  let options = {
    method: method,
    url: BaseUrl + url,
    responseType: 'json',
    crossDomain: true
  }
  // 如果存在自定义config,将自定义参数导入option
  if (config) Object.assign(options,config);
  if (BaseUrl && BaseUrl.split('/')[3] == 'api') {
    // 跨域拦截
    options.url = "/api/" + url
  }
  if (url && url.split('/')[0] == 'demo') {
    // mock拦截
    options.url = url
  }
  if (method === "POST") {
    options.data = data;
  }else if(method === "PUT"){
    options.data = data;
  }
  else {
    options.params = data;
  }
  return new Promise((resolve, reject) => {
    axios(options).then(res => {
      const {status,data} = res;
      if(status === 200){
        let resolveData = responseJsonParse(data);
        switch (resolveData.code) {
        // token已过期
        case 20001:
          localStorage.setItem('user-loginInfo',"{}");
          store.commit("loginInfoSet","");
          // Vue.prototype.$message.error("登录状态已过期,请重新登录");
          Vue.prototype.$message.error(resolveData.message);
          router.push("/login")
          break;
          // token不存在
        case 20002:
          Vue.prototype.$message.error(resolveData.message);
          break;
        default:
          resolve(resolveData);
        }
      }else{
        let rejectData = responseJsonParse(data);
        reject(rejectData);
      }
    }).catch(err => {
      console.warn(err);
      Vue.prototype.$message.error("网络不给力,请稍后再试");
      reject();
    })
  })
}
export default http;
