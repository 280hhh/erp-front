import {
  http,
  apiPath
} from "@/api"
import AddBomDialog from "./dialog/addBomDialog/index.vue"
import UpdBomDialog from "./dialog/updBomDialog/index.vue"
import ShowBomDialog from "./dialog/showBomDialog/index.vue"
import AddBomTypeDialog from "./dialog/addBomTypeDialog/index.vue"
import UpdBomTypeDialog from "./dialog/updBomTypeDialog/index.vue"
import DelBomTypeDialog from "./dialog/delBomTypeDialog/index.vue"

export default {
  name: "BOM",
  data() {
    return {
      treeData: [
        {
          bomTypeName: "BOM组",
          bomTypeId: "",
          children: []
        }
      ],
      defaultProps: {
        children: "children",
        label: "bomTypeName"
      },
      //BOM表格
      BOMSimpleField: [],
      BOMSimpleData: [],
      BOMDataLoading: false,
      menuVisible: false,
      //BOM单内容
      //BOM单表单内容表格头
      BOMFormFiled: [],
      //BOM单表单内容表格数据
      //BOM单表格内容表格头
      BOMTableFiled: [],
      //BOM单表格内容表格数据
      BOMTableList: [],
      BOMTableListLoading: false,
      rightSelectType: [],
      leftSelectType: [],
      page:{
        pageSize:10,
        currentPage:1,
        total:100
      },
      selectBom:[],
      state:''
    };
  },
  methods: {
    //BOM组树
    async getTreeData() {
      http(apiPath.material.bomTypeList, {} , "GET").then(res => {
        const {result} = res;//data.result;
        this.treeData[0].children = [];
        for (let resultKey in result) {
          this.treeData[0].children.push(result[resultKey]);
        }
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //BOM表
    async getBOMData(bomTypeId) {
      this.BOMDataLoading = true;
      let params = Object.assign({bomTypeId})
      http(apiPath.material.bomGetByTypeId(this.page.currentPage,this.page.pageSize), params , "GET").then(res => {
        this.BOMSimpleField = res.result.table.headers;
        this.BOMSimpleData = res.result.table.bodies;
        this.BOMDataLoading = false;
        this.page.total = res.result.total
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //BOM组树单击触发上边BOM表格数据变化
    handleNodeClick(data) {
      this.selectBom=[];
      //打印console.log(data);
      this.leftSelectType = data;
      this.getBOMData(this.leftSelectType.bomTypeId);
    },
    //BOM树右击增删改
    rightHandleNodeContextMenu(MouseEvent, data) {
      this.rightSelectType = data;
      console.log(this.rightSelectType);
      //鼠标右击触发事件
      this.menuVisible = false;
      //先把模态框关死，目的是第二次或者第N次右键鼠标的时候它的默认值是true
      this.menuVisible = true;
      //显示模态窗口，跳出自定义菜单栏
      var menu = document.querySelector("#menu");
      document.addEventListener("click", this.foo);
      //给整个document添加监听鼠标事件，点击任何位置执行foo方法
      menu.style.display = "block";
      menu.style.left = MouseEvent.clientX - 0 + "px";
      menu.style.top = MouseEvent.clientY - 200 + "px";
    },
    //取消鼠标监听事件菜单栏
    foo() {
      this.menuVisible = false;
      document.removeEventListener("click", this.foo);
      //及时关掉监听！
    },
    //弹窗Dialog
    openDialog(name) {
      console.log(this.$refs[name]);
      this.$refs[name].toggle(this.rightSelectType);
    },
    //新增bom弹窗
    addBomDialog(name) {
      console.log(this.leftSelectType);
      this.$refs[name].toggle(this.leftSelectType);
    },
    //修改bom弹窗
    updBomDialog(name) {
      console.log(this.selectBom);
      if(this.selectBom.length==0)
      {
        this.$message('请选择bom后再进行修改'); return;
      }
      this.$refs[name].toggle(this.selectBom);
    },
    //选择页的大小
    handleSizeChange(val){
      this.page.pageSize=val
      this.getBOMData(this.leftSelectType.bomTypeId);
    },
    //选择页码
    handleCurrentChange(val){
      this.page.currentPage=val
      this.getBOMData(this.leftSelectType.bomTypeId);
    },
    //单击选择bom
    clickSelectBom(row){
      this.selectBom=row
    },
    //双击展示bom
    showBom(row){
      console.log(row)
      this.$refs['showBomDialog'].toggle(row);
    },
    //删除BOM
    async deleteBom(){
      if(this.selectBom.length===0){ this.$message('请选择bom后再进行物料的删除'); return;}
      console.log(this.selectBom)
      let params = Object.assign({bomId: this.selectBom.bomId})
      http(apiPath.material.bomDelete, params , "DELETE").then(() => {
        this.getBOMData(this.leftSelectType.bomTypeId);
        this.$message.success('删除成功！');
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //选择物料后
    async handleSelect(){
      this.BOMDataLoading = true;
      let  params=Object.assign({
        keyWord:  this.state})
      http(apiPath.fuzzy.getByKeyWord(this.page.currentPage,this.page.pageSize), params , "GET").then(res => {
        this.BOMSimpleField = res.result.table.headers;
        this.BOMSimpleData = res.result.table.bodies;
        this.BOMDataLoading = false;
        this.page.total=res.result.total
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //对物料进行模糊查询
    async querySearchAsync(queryString, cb) {
      this.queryWarehouseName = queryString;
      this.page.currentPage = 1;
      this.page.pageSize = 20;
      console.log(queryString)
      let  params=Object.assign({page: this.page.currentPage,
        size: this.page.pageSize,
        keyword: queryString})
      http(apiPath.material.materialByKeyWord, params , "GET").then(res => {
        let result = res.result.table.bodies; //data是后端返回的数据
        console.log(result);
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          cb(result);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
  },
  mounted() {
    this.getTreeData();
  },
  components: {
    AddBomDialog,
    UpdBomDialog,
    ShowBomDialog,
    AddBomTypeDialog,
    UpdBomTypeDialog,
    DelBomTypeDialog,
  }
};
