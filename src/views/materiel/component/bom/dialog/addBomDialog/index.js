import {
  http,
  apiPath
} from "@/api"
// import Utils from '../../../basic/util'
export default {
  data() {
    return {
      //选择是与否
      isIndeterminate: false,   //默认全选
      dialogFormVisible: false,
      ruleForm: {
        bomId:'',
        materialName:'',
        bomTypeName:'',
        formNumber:'',
        auditStatus:'',
        status:'',
        formSource:'',
        number:'',
        cost:'',
        unitName:'',
        remark:'',
        bomDetailShines:[
          {materialName:'', materialCode:'',status:'',dosage:'',lossRate:'',unitName:'',remark:''},
        ],
      },
      sumitRuleForm: {
        bomId:'',
        materialName:'',
        bomTypeName:'',
        formNumber:'',
        auditStatus:'',
        status:'',
        formSource:'',
        number:'',
        cost:'',
        unitName:'',
        remark:'',
        bomDetailShines:[
        ],
      },
      id:2,
      rules:{
        materialName: [
          { required: true, message: '请选择物料名', trigger: 'change' }
        ],
        materialCode:[
          { required: true, message: '请输入物料代码', trigger: 'change' }
        ],
        unitName:[
          { required: true, message: '请输入单位', trigger: 'change' }
        ],
        number:[
          { required: true, message: '请输入合成物料数量', trigger: 'change' }
        ],
        cost:[
          { required: true, message: '请输入费用', trigger: 'change' }
        ],
      },
      tempCols: [
        {
          prop: 'materialName',
          label: '物料名字(*)'
        },
        {
          prop: 'materialCode',
          label: '物料代码(*)'
        },
        {
          prop: 'status',
          label: '物料使用状态'
        },
        {
          prop: 'dosage',
          label: '用量(*)'
        },
        {
          prop: 'lossRate',
          label: '损耗率(*)'
        },
        // {
        //   prop: 'warehouseName',
        //   label: '发料仓库(*)'
        // },
        {
          prop: 'unitName',
          label: '单位(*)'
        },
        {
          prop: 'remark',
          label: '备注'
        },
      ],
      BomField:[],
      dialogVisible: false,
      row:[],
    }
  },
  methods: {
    //调用toggle打开窗口
    toggle(selectType) {
      this.row=selectType
      console.log(selectType)
      if(selectType.length===0){
        this.$message('请选择bom组后再进行物料的添加');
        return;}
      this.ruleForm.bomTypeName=selectType.bomTypeName
      this.dialogVisible = true;
    },
    async createBom(){
      http(apiPath.material.bomField, {} , "GET").then(res => {
        this.BomField=res.result
        //console.log(this.bomField)
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //删行
    // eslint-disable-next-line no-unused-vars
    deleteRow(index, rows) {
      this.ruleForm.bomDetailShines.splice(index, 1);
    },
    //添行
    addRow(){
      const newRow={materialName:'', materialCode:'',status:'',dosage:'',lossRate:'',unitName:'',remark:''};
      this.ruleForm.bomDetailShines.push(newRow);
    },
    //选择物料后，回填物料代码
    handleSelect(item,index) {
      console.log(item);
      index.materialCode=item.materialCode;
    },
    //提交数据
    onSumit(ref){
      console.log(ref)
      let pass=true;
      //检验数据是否已经全部填好
      this.ruleForm.bomDetailShines.forEach(item => {
        if(item.materialName===''||item.materialCode===''||item.dosage===''||item.unitName===''||item.lossRate===''){
          this.$message.error('请将表格的必填项全部填入后才能创建bom单');pass=false;return;}
      })
      if(pass===false)return;
      //转化数据的格式才能提交（删除id，并且返回父结点）
      for(var element in this.ruleForm){
        this.sumitRuleForm[element]=this.ruleForm[element]
      }
      console.log(this.sumitRuleForm)     
      this.$refs[ref].validate((valid) => {
        if(valid){
          // var {sumitRuleForm} = this.sumitRuleForm
          //console.log(this.sumitRuleForm) 
          http(apiPath.material.bomCreate, this.sumitRuleForm , "POST").then(res => {
            this.$message.success(res.message);
            //  this.$emit("func",'1')
            this.dialogVisible=false

          }).catch(err => {
            // console.log(err)
            this.$message({
              message: err.message.content,
              type: 'error'
            });
          })
        }
        else
          this.$message({showClose: true, message: '提交失败',type: 'error',offset:40});
      });
        
    },
    //模糊查询物料名
    async searchMaterialAsync(queryString,callback) {
      let params = Object.assign({ keyword:queryString })
      http(apiPath.fuzzy.material, params , "GET").then(res => {
        const {result} = res;//data是后端返回的数据
        clearTimeout(this.timeout);//限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(result);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询仓库名
    async searchWarehouseNameAsync(queryString, callback) {
      let params = Object.assign({ keyWord:queryString })
      http(apiPath.fuzzy.listWarehouseByKeyWord, params , "GET").then(res => {
        const {result} = res;
        clearTimeout(this.timeout);//限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(result);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询计量单位
    async searchUnitAsync(queryString,callback) {//
      let params = Object.assign({ keyword:queryString })
      http(apiPath.fuzzy.unit, params , "GET").then(res => {
        const {result} = res;//data是后端返回的数据
        clearTimeout(this.timeout);//限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(result);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    }
  },

  mounted() {

  }
}
