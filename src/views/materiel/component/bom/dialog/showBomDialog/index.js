import {
  http,
  apiPath
} from "@/api"
export default {
  data(){
    return{
      //选择是与否
      dialogFormVisible: false,
      BomField:[],
      id:1,
      //BOM单表格内容表格头
      BOMTableFiled: [],
      //BOM单表格内容表格数据
      BOMTableList: [],
      BOMTableListLoading:true,
    }
  },
  methods:{
    toggle(row){
      this.dialogFormVisible=true;
      this.getData(row.bomId);
    },
    //获取表格内的数据
    async getData(bomId) {
      this.BOMTableListLoading = true;
      console.log(bomId)
      var params= Object.assign({ bomId: bomId })
      http(apiPath.material.bomGetTreeById , params , "GET").then(res => {
        this.BOMTableFiled=res.result.table.headers
        this.BOMTableList=res.result.table.bodies
        console.log(this.BOMTableList)
        this.BOMTableListLoading = false;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
  },
}