// import {
//   http,
//   apiPath
// } from "@/api"
import created from "mixins/created.js"
import scanNum from "./scanNum/index.vue"

export default created({
  props: {
    addTab: Function,
  },
  components: {
    ScanNum:scanNum
  },
  mixins: [],
  data() {
    return {
      restaurants: [
        { "value": "三全鲜食（北新泾店）", "address": "长宁区新渔路144号" },
        { "value": "Hot honey 首尔炸鸡（仙霞路）", "address": "上海市长宁区淞虹路661号" },
        { "value": "新旺角茶餐厅", "address": "上海市普陀区真北路988号创邑金沙谷6号楼113" },
        { "value": "泷千家(天山西路店)", "address": "天山西路438号" },
      ],
      tableHead:[
        {
          prop:'1',
          describe:'物料名称'
        },
        {
          prop:'2',
          describe:'规格型号'
        },
        {
          prop:'3',
          describe:'收料仓库'
        },
        {
          prop:'4',
          describe:'批号'
        },      
        {
          prop:'5',
          describe:'单位'
        },
        {
          prop:'6',
          describe:'生产日期'
        },
        {
          prop:'7',
          describe:'采购日期'
        },
        {
          prop:'8',
          describe:'已录入数量'
        }],
      tableData:[{
        1:'A18左D柱下外饰板',
        2:'7180661AFCC000',
        3:'素材仓',
        4:'-',
        5:'PCS',
        6:'2020-12-22',
        7:'2020-12-22',
        8:'1/20',
      }
      ],
      scanListHead:[
        {
          prop:'1',
          describe:'物料名称'
        },
        {
          prop:'2',
          describe:'rfid'
        }
      ],
      tableData2:[],
      state: ''
    }
  },
  computed: {},
 
  watch: {},
  methods: {
    querySearch(queryString, cb) {
      var results = queryString ? this.restaurants.filter(this.createFilter(queryString)) : this.restaurants;
      // 调用 callback 返回建议列表的数据
      cb(results);
    },
    createFilter(queryString) {
      return (restaurant) => {
        return (restaurant.value.toLowerCase().indexOf(queryString.toLowerCase()) === 0);
      };
    },
    handleSelect(item) {
      console.log(item);
    },
    handleIconClick(ev) {
      console.log(ev);
    },
    openDialog(){
      console.log(this.ref)
      this.$refs['ScanNum'].toggle()
    },
  }
})
