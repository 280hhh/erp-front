import {
  http,
  apiPath
} from "@/api";
export default {
  data() {
    return {
      //是与否
      options: [
        {
          value: "是",
          label: "是",
        },
        {
          value: "否",
          label: "否",
        },
      ],
      autoListName: [
        "employeeNo",
        "warehouseName",
        "materialTypeName",
        "procurementEmployee",
        "baseUnit",
        "purchaseUnit",
        "saleUnit",
        "stockUnit",
        "unitGroup",
      ],
      //列表
      createLogisticsBaseDto: [
        {
          name: "单价显示精度",
          describe: "accuracyUnitPrice",
          accuracyUnitPrice: "",
          type: "el-input",
        },
        {
          name: "是否自动产生批号",
          describe: "autoGenerateNumber",
          autoGenerateNumber: "",
          type: "el-select",
        },
        {
          name: "是否进行保质期管理",
          describe: "expirationManagement",
          expirationManagement: "",
          type: "el-select",
        },
        {
          name: "毛利率（%）",
          describe: "grossMargin",
          grossMargin: "",
          type: "el-input",
        },
        {
          name: "存货科目代码",
          describe: "inventoryAccountCode",
          inventoryAccountCode: "",
          type: "el-input",
        },
        {
          name: "是否采用业务批次管理",
          describe: "management",
          management: "",
          type: "el-input",
        },
        {
          name: "保质期（天）",
          describe: "materialAge",
          materialAge: "",
          type: "el-input",
        },
        {
          name: "计划单价",
          describe: "planUnitPrice",
          planUnitPrice: "",
          type: "el-input",
        },
        {
          name: "计价方法",
          describe: "pricingMethod",
          pricingMethod: "",
          type: "el-input",
        },
        {
          name: "采购负责人",
          describe: "procurementEmployee",
          procurementEmployee: "",
          type: "el-autocomplete",
          event: this.name,
        },
        {
          name: "采购单价",
          describe: "purchaseUnitPrice",
          purchaseUnitPrice: "",
          type: "el-input",
        },
        {
          name: "销售成本科目代码",
          describe: "saleCostCode",
          saleCostCode: "",
          type: "el-input",
        },
        {
          name: "销售收入科目代码",
          describe: "saleRevenueCode",
          saleRevenueCode: "",
          type: "el-input",
        },
        {
          name: "销售单价",
          describe: "saleUnitPrice",
          saleUnitPrice: "",
          type: "el-input",
        },
        {
          name: "税率（%）",
          describe: "taxRate",
          taxRate: "",
          type: "el-input",
        },
      ],
      createMaterialBaseDto: [
        {
          name: "数量显示精度",
          describe: "accuracy",
          accuracy: "",
          type: "el-input",
        },
        {
          name: "基本计量单位(*)",
          describe: "baseUnit",
          baseUnit: "",
          type: "el-autocomplete",
          event: this.unitName,
        },
        // {
        //   name: '审核日期',
        //   describe: 'examineDate',
        //   examineDate: '',
        //   date: 'datetime',
        //   type: 'el-date-picker',
        // }, {
        //   name: '审核人员',
        //   describe: 'employeeNo',
        //   employeeNo: '',
        //   type: 'el-autocomplete',
        //   event: this.employeeNo
        // },
        //  {
        //   name: '审核标志',
        //   describe: 'exmineSign',
        //   exmineSign: '666',
        //   type: 'el-input',
        // },
        {
          name: "物料代码(*)",
          describe: "materialCode",
          materialCode: "",
          type: "el-input",
        },
        {
          name: "物料名(*)",
          describe: "materialName",
          materialName: "",
          type: "el-input",
        },
        {
          name: "客户编码",
          describe: "customerCode",
          customerCode: "",
          type: "el-input",
        },
        {
          name: "物料属性",
          describe: "materialProperties",
          materialProperties: "",
          type: "el-input",
        },
        {
          name: "物料备注",
          describe: "materialRemark",
          materialRemark: "",
          type: "el-input",
        },
        {
          name: "规格型号",
          describe: "materialSap",
          materialSap: "",
          type: "el-input",
        },
        {
          name: "物料组别名称(*)",
          describe: "materialTypeName",
          materialTypeName: "",
          type: "el-autocomplete",
          event: this.materialTypeName,
        },
        {
          name: "最高存量",
          describe: "maxSave",
          maxSave: "",
          type: "el-input",
        },
        {
          name: "最少存量",
          describe: "minSave",
          minSave: "",
          type: "el-input",
        },
        {
          name: "采购计量单位(*)",
          describe: "purchaseUnit",
          purchaseUnit: "",
          type: "el-autocomplete",
          event: this.unitName,
        },
        {
          name: "安全库存数量",
          describe: "safeStockQuantity",
          safeStockQuantity: "",
          type: "el-input",
        },
        {
          name: "销售计量单位(*)",
          describe: "saleUnit",
          saleUnit: "",
          type: "el-autocomplete",
          event: this.unitName,
        },
        {
          name: "单箱数",
          describe: "singleContainerNumber",
          singleContainerNumber: "",
          type: "el-input",
        },
        {
          name: "库存记录单位(*)",
          describe: "stockUnit",
          stockUnit: "",
          type: "el-autocomplete",
          event: this.unitName,
        },
        {
          name: "计量单位组(*)",
          describe: "unitGroup",
          unitGroup: "",
          type: "el-autocomplete",
          event: this.unitTypeName,
        },
        {
          name: "默认仓库",
          describe: "warehouseName",
          warehouseName: "",
          type: "el-autocomplete",
          event: this.warehouseName,
        },
      ],
      //用于修改,先物流资料，再基本资料
      message: {
        createLogisticsBaseDto: {
          accuracyUnitPrice: "",
          autoGenerateNumber: "",
          expirationManagement: "",
          grossMargin: "",
          inventoryAccountCode: "",
          management: "",
          materialAge: "",
          planUnitPrice: "",
          pricingMethod: "",
          procurementEmployee: "",
          purchaseUnitPrice: "",
          saleCostCode: "",
          saleRevenueCode: "",
          saleUnitPrice: "",
          taxRate: "",
          mlogisticsId: "",
        },
        createMaterialBaseDto: {
          accuracy: "",
          baseUnit: "",
          // examineDate: "2020-2-2 11:11:11",
          // examineEmployee: "张三",
          // exmineSign: 1,
          materialCode: "",
          materialName: "",
          customerCode: "",
          materialProperties: "",
          materialRemark: "",
          materialSap: "",
          materialTypeName: "",
          maxSave: "",
          minSave: "",
          purchaseUnit: "",
          safeStockQuantity: "",
          saleUnit: "",
          singleContainerNumber: "1",
          stockUnit: "",
          unitGroup: "",
          warehouseName: "",
          materialId: "",
        },
      },
      dialogVisible: false,
      back: [],
    };
  },
  mounted() { },
  methods: {
    //调用toggle打开窗口，并得到返回的数据
    toggle(array) {
      this.dialogVisible = true;
      this.back = array;
      this.getMessage();
    },
    //获取基本资料和物流资料byMaterialId
    getMessage() {
      let params = Object.assign({
        materialId: this.back.materialId,
      });
      //基本
      http(apiPath.materialPage.byId, params, "GET").then((res) => {
        //console.log(res);
        this.message["createMaterialBaseDto"] = res.result;
        this.createMaterialBaseDto.forEach((item) => {
          item[item.describe] = this.message["createMaterialBaseDto"][
            item.describe
          ];
        });
      });
      //物流
      http(apiPath.materialPage.byMaterialId, params, "GET").then(
        (res) => {
          //console.log(res);
          this.message["createLogisticsBaseDto"] = res.result; //物流
          this.createLogisticsBaseDto.forEach((item) => {
            item[item.describe] = this.message["createLogisticsBaseDto"][
              item.describe
            ];
          });
        }
      );
      //   let data = await this.$axios.get(this.$interface.MaterialPage.byId, {
      //     params: { materialId: this.back.materialId },
      //   }); //基本
      //   let data1 = await this.$axios.get(
      //     this.$interface.MaterialPage.byMaterialId,
      //     { params: { materialId: this.back.materialId } }
      //   ); //物流
      //   this.message["createMaterialBaseDto"] = data.result; //基本
      //   this.message["createLogisticsBaseDto"] = data1.result; //物流
      //   this.createLogisticsBaseDto.forEach((item, index, array) => {
      //     item[item.describe] = this.message["createLogisticsBaseDto"][
      //       item.describe
      //     ];
      //   });
      //   this.createMaterialBaseDto.forEach((item, index, array) => {
      //     item[item.describe] = this.message["createMaterialBaseDto"][
      //       item.describe
      //     ];
      //   });
    },
    //修改物料
    updateMaterial() {
      if (this.createMaterialBaseDto.materialName)
        this.createLogisticsBaseDto.forEach((item) => {
          this.message["createLogisticsBaseDto"][item.describe] =
                        item[item.describe];
        });
      this.createMaterialBaseDto.forEach((item) => {
        this.message["createMaterialBaseDto"][item.describe] =
                    item[item.describe];
      });
      // console.log(this.createLogisticsBaseDto);
      let params = Object.assign(this.message["createLogisticsBaseDto"]);
      http(apiPath.materialPage.logisticsModify, params, "PUT");
      let params1 = Object.assign(this.message["createMaterialBaseDto"]);
      http(apiPath.materialPage.materialModify, params1, "PUT").then((res) => {
        console.log(res)
        if (res.result === true) {
          this.$message({ message: "物料修改成功", type: "success" });
          this.$parent.showMaterialList()
        }
        else {
          this.$message({ message: "物料修改失败", type: "error" });
        }
        //this.$parent.showMaterialList()
      });
      //   let data = await this.$axios.put(
      //     this.$interface.MaterialPage.logisticsModify,
      //     this.message["createLogisticsBaseDto"]
      //   );
      //   let data1 = await this.$axios.put(
      //     this.$interface.MaterialPage.materialModify,
      //     this.message["createMaterialBaseDto"]
      //   );

      this.dialogVisible = false;
    },
    //模糊查询工人
    name(queryString, cb) {
      let params = queryString ? Object.assign({size: 50},{keyWord: queryString}) : Object.assign({size: 50})
      http(apiPath.employee.fuzzy, params, "GET").then((res) => {
        console.log(res);
        const { result } = res; //data是后端返回的数据
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          cb(result);
        }, 500 * Math.random());
      });
      //   const data = await this.$axios.get(this.$interface.Employee.list(1, 50), {
      //     params: { keyWord: queryString },
      //   });
      //   const result = data.result; //data是后端返回的数据
      //   clearTimeout(this.timeout); //限定展示出来的时间
      //   this.timeout = setTimeout(() => {
      //     cb(result.list);
      //   }, 500 * Math.random());
    },

    //模糊查询仓库
    async warehouseName(queryString, cb) {
      let params = Object.assign({
        keyWord: queryString,
      });
      http(apiPath.user.fuzzy.listWarehouseByKeyWord, params, "GET").then(
        (res) => {
          console.log(res);
          let { result } = res;
          clearTimeout(this.timeout); //限定展示出来的时间
          this.timeout = setTimeout(() => {
            cb(result);
          }, 1000 * Math.random());
        }
      );
      //   let data = await this.$axios.get(
      //     this.$interface.Fuzzy.listWarehouseByKeyWord,
      //     { params: { keyWord: queryString } }
      //   );
      //   let result = data.result;
      //   clearTimeout(this.timeout); //限定展示出来的时间
      //   this.timeout = setTimeout(() => {
      //     cb(result);
      //   }, 1000 * Math.random());
    },
    //删除物料
    delMaterial() {
      this.$confirm("此操作将永久删除该物料, 是否继续?", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning",
      })
        .then(() => {
          let params = Object.assign({ materialCode: this.back.materialCode });
          http(apiPath.materialPage.delete, params, "DELETE").then(
            (res) => {
              console.log(res);
              //console.log(this.$parent.showMaterialList())
              if (res.result == true) {
                this.$message.success("删除成功");
                this.$parent.showMaterialList()
                this.dialogVisible = false;
              } else {
                this.$message.error(res.result);
              }


            }
          );
          //   let data = await this.$axios.delete(
          //     this.$interface.MaterialPage.delete,
          //     { params: { materialCode: this.back.materialCode } }
          //   );
          //   this.$message.success("删除成功");
        })
        .catch(() => {
          this.$message({
            type: "info",
            message: "已取消删除",
          });
        });
    },
    //模糊查询物料组别groupGetByName
    materialTypeName(queryString, cb) {
      let params = Object.assign({ materialTypeName: queryString });
      http(apiPath.user.fuzzy.listMaterialByKeyWord, params, "GET").then(
        (res) => {
          console.log(res);
          let { result } = res;
          clearTimeout(this.timeout); //限定展示出来的时间
          this.timeout = setTimeout(() => {
            cb(result);
          }, 1000 * Math.random());
        }
      );
      //   let data = await this.$axios.get(
      //     this.$interface.Fuzzy.listMaterialByKeyWord,
      //     { params: { materialTypeName: queryString } }
      //   );
      //   let result = data.result;
      //   clearTimeout(this.timeout); //限定展示出来的时间
      //   this.timeout = setTimeout(() => {
      //     cb(result);
      //   }, 1000 * Math.random());
    },
    //模糊查询计量单位
    async unitName(queryString, cb) {
      let params = Object.assign({ materialTypeName: queryString });
      http(apiPath.materialPage.unit, params, "GET").then((res) => {
        console.log(res);
        let { result } = res;
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          cb(result);
        }, 300 * Math.random());
      });
      //   let data = await this.$axios.get(this.$interface.MaterialPage.unit, {
      //     params: { materialTypeName: queryString },
      //   });
      //   let result = data.result;
      //   clearTimeout(this.timeout); //限定展示出来的时间
      //   this.timeout = setTimeout(() => {
      //     cb(result);
      //   }, 300 * Math.random());
    },
    //模糊查询计量单位组
    async unitTypeName(queryString, cb) {
      let params = Object.assign({ materialTypeName: queryString });
      http(apiPath.materialPage.unitType, params, "GET").then((res) => {
        console.log(res);
        let { result } = res;
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          cb(result);
        }, 300 * Math.random());
      });
      //   let data = await this.$axios.get(this.$interface.MaterialPage.unitType, {
      //     params: { materialTypeName: queryString },
      //   });
      //   let result = data.result;
      //   clearTimeout(this.timeout); //限定展示出来的时间
      //   this.timeout = setTimeout(() => {
      //     cb(result);
      //   }, 300 * Math.random());
    },
  },
};
