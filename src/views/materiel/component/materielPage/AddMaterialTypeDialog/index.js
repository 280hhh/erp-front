import {
  http,
  apiPath
} from "@/api";
export default {
  data() {
    return {
      warehouseMaterialTypeDto: {
        materialTypeCode: '',
        materialTypeName: '',
        materialTypeRemark: ''
      },
      warehouseMaterialTypeDtoRef: {
        materialTypeCode: '',
        materialTypeName: '',
        materialTypeRemark: ''
      },
      dialogVisible: false,
    }
  },
  methods: {
    toggle() {
      this.dialogVisible = true;
    },
    async addMaterialType() {
      let params = Object.assign(this.warehouseMaterialTypeDto);
      http(apiPath.materialPage.groupCreate, params, "POST").then((res) => {
        console.log(res);
        if (res.result == true) {
          this.$message.success("种类添加成功");
          this.$parent.showMaterialGroupList();
          this.dialogVisible = false;
        } else {
          this.$message.error(res.result);
          this.$parent.showMaterialGroupList();
        }
      });
      // let data = await this.$axios.post(this.$interface.MaterialPage.groupCreate,this.warehouseMaterialTypeDto)
      // this.$message({ message: '种类添加成功', type: 'success' });
      // this.$parent.showMaterialGroupList();
      // this.dialogVisible = false;
    }
  }
}