import {
  http,
  apiPath
} from "@/api"
import created from "mixins/created.js"
import AddPower from "./components/addPower/index.vue"
import EditPower from "./components/editPower/index.vue"
import DeleteDepartment from "./../../home/public/Delete/index.vue"

export default created({
  props: {},
  components: {
    AddPower,
    EditPower,
    DeleteDepartment
  },
  mixins: [],
  data() {
    return {
      name: 'template',
      msg: "Welcome to Your Vue.js " + name + " App",
      restaurants: [],
      state: '',
      timeout: null,
      value: true,
      tableData: [],
      tableHead: [
        {
          props: 'id',
          describe: 'ID'
        },
        {
          props: 'pid',
          describe: 'pid'
        },
        {
          props: 'name',
          describe: '权限名'
        },
        {
          props: 'url',
          describe: 'api_url'
        },
        {
          props: 'method',
          describe: 'method'
        },
        {
          props: 'powerDescribe',
          describe: '描述'
        }],
      page: {
        total: 10,
        pageSize: 10,
        currentPage: 1
      },
      loading: true
    }
  },
  computed: {},
  created() {
    this.getPowerList()
  },
  watch: {},
  methods: {
    querySearchAsync(queryString, cb) {
      var params = Object.assign({keyWord: queryString}, {size: this.page.total})
      http(apiPath.account.fuzzyPower, params, "GET").then(res => {
        clearTimeout(this.timeout);
        this.timeout = setTimeout(() => {
          cb(res.result);
        }, 1000 * Math.random());
      }).catch(err => {

        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    createStateFilter(queryString) {
      return (state) => {
        return (state.value.toLowerCase().indexOf(queryString.toLowerCase()) === 0);
      };
    },
    handleSelect(item) {
      console.log(item);
    },
    handleSizeChange(val) {
      this.page.pageSize = val
      this.fresh()
    },
    handleCurrentChange(val) {
      this.page.currentPage = val
      this.fresh()
    },
    openDialog(name, row) {
      console.log(this.$refs[name].toggle(row, this.page))
    },
    handleClick(row) {
      console.log(row);
    },
    openDeleteDialog(row) {
      var params = Object.assign({powerId: row.id})
      this.$refs['deleteDepartment'].toggle('权限', row.name, apiPath.account.powerDelete, params, '确定删除该接口权限？')
    },
    getPowerList() {
      this.loading = true
      var params = Object.assign({keyWord: this.state}, {pageNum: this.page.currentPage}, {pageSize: this.page.pageSize})
      http(apiPath.account.powerList, params, "GET").then(res => {
        console.log(res)
        this.tableData = res.result.records
        this.page.total = res.result.total
        this.loading = false
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    fresh() {///power/getPowerByMenuId
      this.getPowerList()
    },

  }
})
