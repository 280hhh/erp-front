import {
  http,
  apiPath
} from "@/api"
import created from "mixins/created.js"
import AddEmployee from "./components/addEmployee/index.vue"
import EditEmployee from "./components/editEmployee/index.vue"
import RoleAssignment from "./components/roleAssignment/index.vue"
import DeleteDepartment from "@/views/home/public/Delete/index.vue"

export default created({
  props: {},
  components: {
    AddEmployee,
    EditEmployee,
    RoleAssignment,
    DeleteDepartment
  },
  mixins: [],
  data () {
    return {
      loading: true,
      name: 'template',
      msg: "Welcome to Your Vue.js "+ name +" App",
      restaurants: [],
      state: '',
      timeout:  null,
      tableData: [],
      tableHead: [
        {
          props: 'id',
          describe: 'ID'
        },
        {
          props: 'employeeNo',
          describe: '工号'
        },
        {
          props: 'name',
          describe: '姓名'
        },
        {
          props: 'departmentName',
          describe: '部门'
        },
        {
          props: 'jobName',
          describe: '职位'
        },
        {
          props: 'birth',
          describe: '入职日期'
        },
      ],
      page:{
        total:100,
        pageSize:10,
        currentPage:1
      }
    }
  },
  computed: {},
  created () {
    // this.restaurants =
    this.loadAll();
  },
  watch: {},
  methods: {
    //獲取分页查询员工角色信息列表
    loadAll() {
      this.loading = true;
      let params = Object.assign({
        // pageNo: 1,
        // pageSize: 10,
        pageNum: this.page.currentPage,//this.pageNo
        pageSize: this.page.pageSize,//this.pageSize
      });
      http(apiPath.employee.list, params, "GET").then((res) => {
        console.log(res);
        this.tableData = res.result.records;
        this.page.total=res.result.total
        this.loading = false;
      })
    },
    fresh(){
      this.loadAll()
    },
    //模糊查询
    querySearchAsync(queryString, cb) {
      let params = Object.assign({
        keyWord: queryString,
        pageNum: this.page.currentPage,//
        pageSize: this.page.pageSize,//
      })
      http(apiPath.employee.list, params, "GET").then(res => {
        console.log(res)
        // let result = res.result.records;
        clearTimeout(this.timeout);//限定展示出来的时间
        this.timeout = setTimeout(() => {
          cb(res.result.records);
        }, 3000 * Math.random());
      })
      // var results = queryString ? this.restaurants.filter(this.createStateFilter(queryString)) : this.restaurants;

      // clearTimeout(this.timeout);
      // this.timeout = setTimeout(() => {
      //   cb(results);
      // }, 3000 * Math.random());
    },
    createStateFilter(queryString) {
      return (state) => {
        return (state.value.toLowerCase().indexOf(queryString.toLowerCase()) === 0);
      };
    },
    handleSelect(item) {
      console.log(item);
    },
    handleSizeChange(val) {
      console.log(`每页 ${val} 条`);
      this.page.pageSize=val
      this.loadAll()
    },
    handleCurrentChange(val) {
      console.log(`当前页: ${val}`);
      this.page.currentPage=val;
      this.loadAll()
    },
    openEditDialog(row){
      console.log(row)
      this.$refs['editEmployee'].toggle(row)
      //this.$refs[name].toggle(this.tableData[0])
    },
    openDialog(name,row){
      this.$refs[name].toggle(row)
    },
    handleClick(row,value) {
      // row.value=false
      console.log(row,value);
      var params={employeeId:row.id,disable: value}
      http(apiPath.account.employeeChangeStatus, params, "POST",'f').then(res => {
        console.log(res)
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    openDeleteDialog(row){
      var url=apiPath.employee.delete
      var params = Object.assign({
        employeeId: row.id
      });
      this.$refs['deleteDepartment'].toggle('员工',row.name,url,params,"")
    }

  }
})
