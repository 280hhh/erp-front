import {
  http,
  apiPath
} from "@/api"
export default {
  data() {
    return {
      value:[],
      dialogVisible: false,
      data: [],
      row:[]
    };
  },
  methods: {
    handleClose() {

    },
    employeeRoleList() {
      this.loading = true;
      let params = Object.assign({
        'pageNum': 1,
        'pageSize': 10
      });
      http(apiPath.account.employeeRoles, params, "GET").then((res) => {
        console.log(res.result.records);
        console.log(this.value)
        this.data = res.result.records
        this.loading = false;
      })
    },
    //employeegetEmployeeHoldRoles
    getEmployeeHoldRoles() {
      this.loading = true;
      let params = Object.assign({
        employeeId:this.row.id
      });
      http(apiPath.account.employeegetEmployeeHoldRoles, params, "GET").then((res) => {
        console.log(res.result.holdingRole);
        console.log(this.value)
        //  this.value=res.result.unholdingRole
        this.data=res.result.holdingRole
        this.value=res.result.unholdingRole
        this.loading=false
      })
    },
    toggle(row){
      console.log(row)
      this.row=row
      this.getEmployeeHoldRoles()
      this.dialogVisible=!this.dialogVisible
    },
    handleChange(value, direction, movedKeys) {
      if(direction=='right'){
        this.right(movedKeys)
      }else{
        this.left(movedKeys)
      }

    },
    left(key){
      console.log(key);
      var _this=this
      var params=Object.assign({'employeeId':_this.row.id,'rolesId':key})

      console.log(params)
      http(apiPath.account.deleteEmployeeHoldRoles, params, "DELETE",'f').then(res => {
        this.$message({
          message: res.message,
          type: 'success'
        });
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    right(key){
      console.log(key);
      var _this=this
      var params=Object.assign({'employeeId':_this.row.id,'rolesId':key})
      console.log(params)
      http(apiPath.account.addEmployeeHoldRoles, params, "POST").then(res => {
        this.$message({
          message: res.message,
          type: 'success'
        });
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    }
  }
}
