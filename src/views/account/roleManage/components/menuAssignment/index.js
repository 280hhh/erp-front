import {
  http,
  apiPath
} from "@/api"
export default {
  data() {
    return {
      dialogVisible: false,
      value1:true,
      row:[],
      checked:[],
      data : [{
        id: 1,
        label: '一级 1',
        children: [{
          id: 4,
          label: '二级 1-1',
          children: [{
            id: 9,
            label: '三级 1-1-1'
          }, {
            id: 10,
            label: '三级 1-1-2'
          }]
        }]
      }, {
        id: 2,
        label: '一级 2',
        children: [{
          id: 5,
          label: '二级 2-1'
        }, {
          id: 6,
          label: '二级 2-2'
        }]
      }, {
        id: 3,
        label: '一级 3',
        children: [{
          id: 7,
          label: '二级 3-1'
        }, {
          id: 8,
          label: '二级 3-2'
        }]}]
    };
  },
  methods: {
    handleClose() {

    },
    getMenuHoldPowers(){
      var _this=this
      var params = Object.assign({roleId:_this.row.id})
      http(apiPath.account.roleGetMenuTreeVo, params, "GET").then(res => {
        console.log(res.result.menuTree)
        _this.data=res.result.menuTree
        _this.checked=res.result.checked
        _this.$message({
          message: res.message,
          type: 'success'
        });
      }).catch(err => {
        // console.log(err)
        _this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    toggle(row){
      console.log(row)
      this.row=row
      this.getMenuHoldPowers()
      this.dialogVisible=!this.dialogVisible
    },
    change(state){
      console.log(state)
      var _this=this
      var params ='??'
      Object.assign({roleId:_this.row.id})
      http(apiPath.account.addRoleHoldMenus, params, "GET").then(res => {
        console.log(res.result.menuTree)
        _this.data=res.result.menuTree
        _this.checked=res.result.checked
        _this.$message({
          message: res.message,
          type: 'success'
        });
      }).catch(err => {
        // console.log(err)
        _this.$message({
          message: err.message.content,
          type: 'error'
        });
      })

    }
  }
}
