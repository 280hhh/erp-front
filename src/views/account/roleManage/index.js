import {
  http,
  apiPath
} from "@/api"
import created from "mixins/created.js"
import AddRole from "./components/addRole/index.vue"
import EditRole from "./components/editRole/index.vue"
import MenuAssignment from "./components/menuAssignment/index.vue"
import DeleteDepartment from "./../../home/public/Delete/index.vue"

export default created({
  props: {},
  components: {
    AddRole,
    EditRole,
    MenuAssignment,
    DeleteDepartment
  },
  mixins: [],
  data () {
    return {
      loading: true,
      name: 'template',
      msg: "Welcome to Your Vue.js "+ name +" App",
      restaurants: [],
      state: '',
      timeout:  null,
      tableData: [],
      tableHead:[
        {props:'id',
          describe:'ID'
        },
        {props:'name',
          describe:'角色名称'
        },
        {props:'describe',
          describe:'角色描述'
        }],
      page:{
        total:10,
        pageSize:20,
        currentPage:1
      }
    }
  },
  computed: {},
  created () {
    //
    this.loadAll();
  },
  watch: {},
  methods: {
    fresh() {
      this.loadAll()
    },
    loadAll() {
      let params = Object.assign({
        // pageNo: 1,
        // pageSize: 10,
        pageNum: this.page.currentPage,//this.pageNo
        pageSize: this.page.pageSize,//this.pageSize
      });
      this.loading = true;
      http(apiPath.employee.employeeRoles, params, "GET").then((res) => {
        console.log(res);
        this.tableData = res.result.records;
        this.page.total = res.result.total
        this.loading = false;
      })
    },
    querySearchAsync(queryString, cb) {
      let params = Object.assign({
        keyWord: queryString,
        pageNum: this.page.currentPage,//
        pageSize: this.page.pageSize,//
      })
      http(apiPath.employee.employeeRoles, params, "GET").then(res => {
        console.log(res)
        // let result = res.result.records;
        clearTimeout(this.timeout);//限定展示出来的时间
        this.timeout = setTimeout(() => {
          cb(res.result.records);
        }, 3000 * Math.random());
      })
      // var results = queryString ? this.restaurants.filter(this.createStateFilter(queryString)) : this.restaurants;

      // clearTimeout(this.timeout);
      // this.timeout = setTimeout(() => {
      //   cb(results);
      // }, 3000 * Math.random());
    },
    createStateFilter(queryString) {
      return (state) => {
        return (state.value.toLowerCase().indexOf(queryString.toLowerCase()) === 0);
      };
    },
    handleSelect(item) {
      console.log(item);
    },
    handleSizeChange(val) {
      console.log(`每页 ${val} 条`);
      this.page.pageSize = val
      this.loadAll()
    },
    handleCurrentChange(val) {
      console.log(`当前页: ${val}`);
      this.page.currentPage = val;
      this.loadAll()
    },
    openDialog(name,row){
      console.log(row)
      console.log(this.$refs)
      this.$refs[name].toggle(row)
    },
    openDeleteDialog(row){
      // var url = apiPath.employee.roledelete
      console.log(row.id)
      var params = Object.assign({
        roleId: row.id
      });

      this.$refs['deleteDepartment'].toggle('角色', row.name, apiPath.employee.roledelete,params,"")
    }

  }
})
