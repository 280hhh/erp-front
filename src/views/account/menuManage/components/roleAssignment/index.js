import {
  http,
  apiPath
} from "@/api"
export default {
  data() {
    return {
      value:[],
      dialogVisible: false,
      data: [],
      row:{},
      loading:true
    };
  },
  methods: {
    handleClose() {

    },
    toggle(row){
      this.dialogVisible=!this.dialogVisible
      this.row=row
      this.getMenuHoldPowers()
    },
    getMenuHoldPowers(){
      // /menu/getMenuHoldPowers
      var params=Object.assign({menuId:this.row.id})
      this.loading=true
      http(apiPath.account.getMenuHoldPowers, params, "GET").then(res => {
        this.loading=false
        console.log(res.result.HoldPower)
        this.value=res.result.holdingPower
        this.data=res.result.allPower
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    handleChange(value, direction, movedKeys) {
      if(direction=='right'){
        this.right(movedKeys)
      }else{
        this.left(movedKeys)
      }

    },
    left(key){
      console.log(key);
      var _this=this
      var params=key.map(function(el){
        return{
          menuId:_this.row.id,
          powerId:el
        }
      })
      http(apiPath.account.deleteMenuHoldPowers, params, "POST").then(res => {
        this.$message({
          message: res.message,
          type: 'success'
        });
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    right(key){
      console.log(key);
      var _this=this
      var params=key.map(function(el){
        return{
          menuId:_this.row.id,
          powerId:el
        }
      })
      http(apiPath.account.addMenuHoldPowers, params, "POST").then(res => {
        this.$message({
          message: res.message,
          type: 'success'
        });
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    }

  }
}
