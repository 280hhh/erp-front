import {
  http,
  apiPath
} from "@/api"
export default {
  data() {
    return {
      dialogVisible: false,
      form: {
        name: '',
        pid:0,
        path:'',
        menuDescribe:'',
        id:''
      },
      rules: {
        name: [
          { required: true, message: '请输入部门名称', trigger: 'blur' },
        ],
        path: [
          { required: true, message: '请输入部门名称', trigger: 'blur' },
        ]
      },
      row:{},

    };
  },
  methods: {
    handleClose() {

    },
    toggle(row){
      this.row=row
      this.row
      this.init(row)
      this.dialogVisible=!this.dialogVisible
    },
    onSubmit(){
      console.log(this.form)
      this.$refs['form'].validate((valid) => {
        if (valid) {
          http(apiPath.account.menuUpdate, this.form, "POST").then(res => {
            console.log(res)
            this.$parent.fresh()
            this.$message({
              message: res.message,
              type: 'success'
            });
            this.dialogVisible=false
          }).catch(err => {
            // console.log(err)
            this.$message({
              message: err.message.content,
              type: 'error'
            });
          })
        } else {
          console.log('error submit!!');
          return false;
        }
      });
    },
    init(row){
      this.form.id=row.id
      this.form.name=row.name
      this.form.menuDescribe=row.menuDescribe
      this.form.path=row.path
      this.form.pid=row.pid
      console.log(this.form)
    },
  }
}
