import {
  http,
  apiPath
} from "@/api"
export default {
  data() {
    return {
      dialogVisible: false,
      form: {
        name: '',
        remarks: '',
        pid:'',
        id:'',
        code:''
      },
      options: [{
        value: 'zhinan',
        label: '指南',
        children: [{
          value: 'shejiyuanze',
          label: '设计原则',
          children: [{
            value: 'yizhi',
            label: '一致'
          }, {
            value: 'fankui',
            label: '反馈'
          }, {
            value: 'xiaolv',
            label: '效率'
          }, {
            value: 'kekong',
            label: '可控'
          }]
        }]}],
      row:{},
      rules: {
        name: [
          { required: true, message: '请输入部门名称', trigger: 'blur' },
        ],
        code: [
          { required: true, message: '请输入部门代码', trigger: 'blur' },
        ],
      },
    };
  },
  methods: {
    handleClose() {

    },
    toggle(row){
      this.dialogVisible=!this.dialogVisible
      this.row=row
      this.init(row);
    },
    getTree(){
      // departmentTree
      http(apiPath.account.departmentTree, {}, "GET").then(res => {
        this.options=res.result
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    init(row){
      this.form.name=row.name
      this.form.remarks=row.remarks
      this.form.id=row.id
      this.form.pid=row.pid
      this.form.code=row.code
      console.log(this.form)
    },
    onSubmit(){
      this.$refs['form'].validate((valid) => {
        if (valid) {
          http(apiPath.account.departmentUpdate, this.form, "POST").then(res => {
            //this.options=res.result
            var row={name:'',remarks:'',id:'',pid:'',code:''}
            this.init(row)
            console.log(res.result)
            this.$message({
              message: res.message,
              type: 'success'
            });
            this.$parent.fresh()
            this.dialogVisible = false
          }).catch(err => {
            // console.log(err)
            this.$message({
              message: err.message.content,
              type: 'error'
            });
          })
        } else {
          console.log('error submit!!');
          return false;
        }
      });

    },
    getPid(item){
      console.log(item)
      this.form.pid=item[item.length-1]
    }
  },
  mounted(){
    this.getTree()
  }
}
