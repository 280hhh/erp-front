import created from "mixins/created.js"

export default created({
  props: {
    menuList: {
      type: Array,
      require: true
    }
  },
  data() {
    return {
      defaultActiveUrl: "",
      defaultActive: ""
    }
  },
  created() {
    this.init();
  },
  methods: {
    init() {
      this.defaultActiveUrl = this.$route.path;
      console.log("defaultActiveUrl:", this.defaultActiveUrl)
      console.log(this.menuList)
      this.defaultActive=this.menuList[0].children[0].index
      console.log(this.defaultActive)
    }
  }
})
