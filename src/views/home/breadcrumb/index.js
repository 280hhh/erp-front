import VuexHome from "mixins/vuex/home.js"

export default {
  props: {},
  components: {},
  mixins: [VuexHome],
  data () {
    return {
      dataProps: ''
    }
  },
  computed: {
    breadName(){
      let {breadCrumbGet} = this;
      let name = breadCrumbGet && breadCrumbGet.meta && breadCrumbGet.meta.name;
      return name||""
    }
  },
  created () {},
  watch: {},
  methods: {
    
  }
}