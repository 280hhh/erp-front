/* eslint-disable max-len */
import {
  http,
  apiPath
} from "@/api"
import HelloWorld from 'components/HelloWorld'
import created from "mixins/created.js"
import showinstockother from '../showbill/showinstockother/index.vue'
import showinstockproduct from '../showbill/showinstockproduct/index.vue'
import showinstockprofits from '../showbill/showinstockprofits/index.vue'
import showinstockpurchase from '../showbill/showinstockpurchase/index.vue'
import showoutstockdraw from '../showbill/showoutstockdraw/index.vue'
import showoutstockloss from '../showbill/showoutstockloss/index.vue'
import showoutstockother from '../showbill/showoutstockother/index.vue'
import showoutstocksale from '../showbill/showoutstocksale/index.vue'
import resultdialog from '../../resultdialog/index.vue'

export default created({
  props: {},
  components: {
    HelloWorld,
    showinstockother,
    showinstockproduct,
    showinstockprofits,
    showinstockpurchase,
    showoutstockdraw,
    showoutstockloss,
    showoutstockother,
    showoutstocksale,
    resultdialog
  },
  mixins: [],
  data() {
    return {
      headers: [],
      bodies: [],
      loading: false,
      select: [],
      page: {
        currentPage: 1,
        pageSize: 10,
        total: 100
      },
      itemdocumentNo: "",
      dialogVisible: false,
      dialogData: [],
    }
  },
  computed: {},
  created() {},
  destroyed() {},
  watch: {},
  methods: {
    async getDate() {
      this.loading = true;
      let params = Object.assign({ page: this.page.currentPage, size: this.page.pageSize, auditStatus: '未审核' })
      http(apiPath.examine.billList, params, "GET").then(res => {
        const { headers } = res.result.table;
        const { bodies } = res.result.table;
        this.headers = headers;
        this.bodies = bodies;
        this.page.total = res.result.total;
        this.loading = false;
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //发送审核单据单号
    async pass() {
      if (this.select.length === 0) { this.$message.error('请选择单据后再进行审核'); return; }
      http(apiPath.examine.examineBillList, this.select, "PUT").then(res => {
        console.log(res)
        this.$message.success(res.message);
        this.$refs.resultdialog.results = res.result;
        this.$refs.resultdialog.toggle();
        this.getDate();
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    async getBillbyNO(documentNo) {
      http(apiPath.bills.bills(documentNo), {}, "GET").then(res => {
        this.dialogData = res.result.headers;
        console.log(res);
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //点击某行打开窗口
    rowClick(row) {

      this.dialogVisible = true;
      switch (row.documentType) {
      case '其他入库':
        this.$refs['showinstockother'].toggle(row.documentNo);
        break;
      case '产品入库':
        this.$refs['showinstockproduct'].toggle(row.documentNo);
        break;
      case '盘盈入库':
        this.$refs['showinstockprofits'].toggle(row.documentNo);
        break;
      case '采购入库':
        this.$refs['showinstockpurchase'].toggle(row.documentNo);
        break;
      case '领料出库':
        this.$refs['showoutstockdraw'].toggle(row.documentNo);
        break;
      case '盘亏毁损':
        this.$refs['showoutstockloss'].toggle(row.documentNo);
        break;
      case '其他出库':
        this.$refs['showoutstockother'].toggle(row.documentNo);
        break;
      case '销售出库':
        this.$refs['showoutstocksale'].toggle(row.documentNo);
        break;
      }
    },
    //改变每页大小
    handleSizeChange(val) {
      this.page.pageSize = val
      this.getDate();
    },
    //跳转页数
    handleCurrentChange(val) {
      this.page.currentPage = val
      this.getDate();
    },
    getSelect(val) {
      this.select = [];
      val.forEach(element => {
        this.select.push(element.documentNo)
      });
      console.log(this.select);
    }
  },
  mounted() {
    this.getDate();
  }
})