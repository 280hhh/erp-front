/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable max-len */
import created from "mixins/created.js"
import showplandialog from "./showplandialog/index.vue"
import resultdialog from "../resultdialog/index.vue"
import {
  http,
  apiPath
} from "@/api"
export default created({
  props: {},
  components: {
    showplandialog,
    resultdialog
  },
  mixins: [],
  data() {
    return {
      name: 'template',
      msg: "Welcome to Your Vue.js " + name + " App",
      examinePlanNavArr: ["我的待审核", "所有任务", "待审核", "审核通过", "被驳回", "所有未删除的任务表"], //侧边栏生产

      dialogTableVisible: false,
      examinePlanListLoading: false,
      selectExaminePlanType: "", //侧边栏选择任务类型
      examinePlanList: [], //请求获取任务列表
      selectExaminePlanRow: [],
      examinePlanPageInfo: { total: 100, currentPage: 1, pageSize: 10 }, //请求获取任务列表的分页信息
      examineTaskPageInfo: { total: 0, currentPage: 1, pageSize: 10 },
      examinePlanTableFiled: [
        { key: "id", label: "标识" },
        { key: "createEmployeeNo", label: "创建人" },
        { key: "principalEmployeeNo", label: "负责人" },
        { key: "approvedEmployeeNo", label: "审批人" },
        { key: "productionNo", label: "任务单号" },
        { key: "state", label: "任务状态" },
        { key: "status", label: "状况" },
        { key: "createDate", label: "创建时间" },
        { key: "auditStatus", label: "审核状态" },
        { key: "emergencyLevel", label: "紧急程度" },
        { key: "closingDate", label: "关闭时间" },
        { key: "source", label: "来源" },
        { key: "linkedOrder", label: "关联单号" },
        { key: "remarks", label: "备注" }
      ], //映射任务表格label与后台字段
      examineTaskListLoading: false,
      examineTaskTableFiled: [
        { key: "id", label: "标识" },
        { key: "taskName", label: "名称" },
        { key: "state", label: "状态" },
        { key: "operatorEmployeeNo", label: "操作人" },
        { key: "forecastStartDate", label: "预计开始时间" },
        { key: "stateChangeDate", label: "最新状态变化时间" },
        { key: "forecastEndDate", label: "预计结束时间" },
        { key: "productionName", label: "成品名称" },
        { key: "productionNumber", label: "成品数量" }
      ], //映射工序表格label与后台字段
      examineTaskList: [],
      examineMessgae: {
        productionNo: '',
        planAuditStatusEnum: '',
      }
    }
  },
  computed: {},
  created() {},
  destroyed() {},
  watch: {},
  methods: {
    //选择菜单栏
    // eslint-disable-next-line no-unused-vars
    examinePlanHandleSelect(key, keyPath) {
      this.examinePlanPageInfo.currentPage = 1;
      this.examinePlanPageInfo.pageSize = 10;
      this.selectExaminePlanType = key
      this.getExaminePlans(key)
    },
    //根据菜单栏展示列表
    async getExaminePlans(key) { //myPendingPlans
      if (key === '我的待审核') {
        this.examinePlanListLoading = true;
        http(apiPath.examine.myPendingPlans(
          this.examinePlanPageInfo.currentPage, this.examinePlanPageInfo.pageSize), {}, "GET").then(res => {
          this.examinePlanList = res.result.data;
          this.examinePlanPageInfo.total = res.result.total;
          this.examinePlanListLoading = false;

        }).catch(err => {
          // console.log(err)
          this.$message({
            message: err.message.content,
            type: 'error'
          });
        })
        return;
      }
      if (key === '所有未删除的任务表') {
        this.examinePlanListLoading = true;
        http(apiPath.examine.plans(this.examinePlanPageInfo.currentPage, this.examinePlanPageInfo.pageSize), {}, "GET").then(res => {
          this.examinePlanList = res.result.data;
          this.examinePlanPageInfo.total = res.result.total;
          this.examinePlanListLoading = false;

        }).catch(err => {
          // console.log(err)
          this.$message({
            message: err.message.content,
            type: 'error'
          });
        })
        return;
      }
      if (key === '所有任务')
        key = ''
      console.log(this.examinePlanPageInfo)
      this.examinePlanListLoading = true;
      // eslint-disable-next-line max-len
      let params = Object.assign({ planAuditStatusEnum: key, pageNum: this.examinePlanPageInfo.currentPage, pageSize: this.examinePlanPageInfo.pageSize })
      http(apiPath.examine.auditStatusPlans, params, "GET").then(res => {
        this.examinePlanList = res.result.data;
        this.examinePlanPageInfo.total = res.result.total
        this.examinePlanListLoading = false;
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })

    },
    //展示工序信息
    async selectExaminePlan(row) {
      //taskInfo
      this.selectExaminePlanRow = row
      this.examineTaskList = []
      // eslint-disable-next-line max-len
      http(apiPath.examine.taskInfo(row.productionNo, this.examineTaskPageInfo.currentPage, this.examineTaskPageInfo.pageSize), {}, "GET").then(res => {
        this.examineTaskList = res.result.data
        console.log(res)
        this.examineTaskPageInfo.total = res.result.total
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    showplandialog(row) {
      console.log(row);
      this.selectExaminePlanRow = row;
      this.$refs['showplandialog'].toggle(this.selectExaminePlanRow);
    },
    //打勾时促发
    selectExaminePlans(val) {
      this.select = []
      val.forEach(element => {
        this.select.push(element.productionNo)
      });
      console.log(this.select)
    },
    //改变大小
    examinePlanPageSizeChange(val) {
      this.examinePlanPageInfo.pageSize = val;
      this.getExaminePlans();
    },
    //改变页位
    examinePlanPageCurrentChange(val) {
      this.examinePlanPageInfo.currentPage = val;
      this.getExaminePlans();
    },
    // 得到任务(即工序)
    async getExamineTask(productionNo, pageNum, pageSize) {
      this.examineTaskListLoading = true;
      let params = Object.assign({ productionNo: productionNo })
      http(apiPath.examine.TaskInfo.all(pageNum, pageSize), params, "GET").then(res => {
        this.examineTaskList = res.result.records;
        if (this.examineTaskList.length === 0) { this.$message.info('该计划下没有生产任务'); }
        this.examineTaskListLoading = false;
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    openDialog(name) {
      console.log(this.$refs[name]);
      this.$refs[name].toggle();
    },
    //审核通过与驳回
    async examine(kind) {
      // this.$message.error('请选择计划后再进行审核');

      if (!this.select) {
        this.$message.error('请选择计划后再进行审核');
        return;
      }
      this.examineMessgae.planAuditStatusEnum = kind
      // eslint-disable-next-line max-len
      let params = Object.assign({ planBatchExamineRequestVos: this.select, planAuditStatusEnum: this.examineMessgae.planAuditStatusEnum })
      http(apiPath.examine.batchExamine, params, "POST").then(res => {
        this.$message.success(res.message);
        this.$refs.resultdialog.results = res.result;
        this.$refs.resultdialog.toggle();
        this.select = []

      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
      return;
    },
  },
  mounted() {
    this.examinePlanHandleSelect('我的待审核', '1')
  }
})