export function getLoginInfo() {
  const info = localStorage.getItem("user-loginInfo");
  if (info) {
    return JSON.parse(info);
  } else {
    return {};
  }
}

export function returnState(state, key) {
  let info = state.loginInfoState,
    data;
  if (info.username) {
    data = info[key];
  } else {
    data = getLoginInfo()[key];
  }
  return data;
}

export function addChildren(el) {
  el.index = el.path;
  if (el.children) {
    // el.children.index = el.children.path;
    el.children.forEach(el=>{
      el.index = el.path;
      if (el.children) {
        // el.children.index = el.children.path;
        el.children.forEach(el=>{
          el.index = el.path;
        })
      }
    })
  }
}
