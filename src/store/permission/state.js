/*
* 菜单级别权限限制
* params: power
* 1:所有权限
* 2:add、edit、delete
* 3:add、edit
* */
const state = {
  // 权限限制
  permissionState:{
    "/user":{
      name:"用户管理",
      power:1
    },
    "/permission":{
      name:"权限管理",
      power:1
    }
  },
  // 允许查看菜单列表
  menuState:[],
};

export default state;
