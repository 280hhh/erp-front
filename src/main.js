import Vue from 'vue'
import router from './router'
import 'common/styles/index.less'
import Route from './views/route/index.vue'
import store from './store'
import permission from 'common/directive/permission'

// import ElementUI from 'element-ui'
// Vue.use(ElementUI)
import {
  Pagination,
  Button,
  Select,
  Form,
  FormItem,
  Input,
  Table,
  TableColumn,
  RadioGroup,
  RadioButton,
  Menu,
  Submenu,
  MenuItem,
  MenuItemGroup,
  Collapse,
  CollapseItem,
  Dialog,
  Popover,
  Message,
  Loading,
  DatePicker,
  CheckboxGroup,
  Checkbox,
  Radio,
  Row,
  Col,
  Option,
  MessageBox,
  Tabs,
  TabPane,
  PageHeader,
  Upload,
  Autocomplete,
  Badge,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Tooltip,
  Container,
  Aside,
  Main,
  Divider,
  Tree,
  Header,
  Cascader,
  Switch,
  Transfer,
  breadcrumb,
  breadcrumbItem,
  Popconfirm,
  Card
} from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import QlButton from 'components/qlButton/button'

let components = [
  // eslint-disable-next-line max-len
  Pagination,
  Select,
  Button,
  Form,
  FormItem,
  Input,
  Table,
  TableColumn,
  RadioGroup,
  RadioButton,
  Menu,
  Submenu,
  MenuItem,
  MenuItemGroup,
  Collapse,
  CollapseItem,
  Dialog,
  Popover,
  DatePicker,
  CheckboxGroup,
  Checkbox,
  Radio,
  Row,
  Col,
  Option,
  Tabs,
  TabPane,
  PageHeader,
  Upload,
  Autocomplete,
  Badge,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Tooltip,
  Container,
  Aside,
  Main,
  Divider,
  Tree,
  Header,
  Cascader,
  Switch,
  Transfer,
  breadcrumb,
  breadcrumbItem,
  Popconfirm,
  QlButton,
  Card
]

components.forEach(el => {
  Vue.component(el.name, el);
  // Vue.use(el);
})

Vue.prototype.$tabMenu = [
  { path: 'bills/inStockPurchase', name: 'in-stockroom-purchase', describe: '采购入库', group: "materiel" },
  { path: 'bills/inStockProduct', name: 'in-stockroom-product', describe: '产品入库', group: "materiel" },
  // {path:'bills/inStockProfits',name: 'in-stockroom-profits', describe: '盘盈入库',group:"materiel"},
  { path: 'bills/inStockOther', name: 'in-stockroom-other', describe: '其他入库', group: "materiel" },
  { path: 'bills/outStockSale', name: 'out-stockroom-sale', describe: '销售出库', group: "materiel" },
  { path: 'bills/outStockDraw', name: 'out-stockroom-draw', describe: '领料出库', group: "materiel" },
  // {path:'bills/outStockLoss',name: 'out-stockroom-loss', describe: '盘亏毁损',group:"materiel"},
  { path: 'bills/outStockOther', name: 'out-stockroom-other', describe: '其他出库', group: "materiel" },
  {path:'timeBooks/purchaseEntryTimeBook',name: 'purchase-entry-time-book', describe: '采购入库序时簿',group:"Materiel"},
  {path:'timeBooks/productEntryTimeBook',name: 'product-entry-time-book', describe: '产品入库序时簿',group:"Materiel"},
  // {path:'InventoryEntryTimeBook',name: 'inventory-entry-time-book', describe: '盘盈入库序时簿',group:"Materiel"},
  { path: 'timeBooks/otherEntryTimeBook', name: 'other-entry-time-book', describe: '其他入库序时簿', group: "Materiel" },
  { path: 'timeBooks/sellOutTimeBook', name: 'sell-out-time-book', describe: '销售出库序时簿', group: "Materiel" },
  { path: 'timeBooks/getMaterialsOutTimeBook', name: 'get-materials-out-time-book', describe: '领料出库序时簿', group: "Materiel" },
  // {path:'DamagedDishDeficientTimeBook',name: 'damaged-dish-deficient-time-book', describe: '盘亏毁损序时簿',group:"Materiel"},
  { path: 'timeBooks/otherOutTimeBook', name: 'other-out-time-book', describe: '其他出库序时簿', group: "Materiel" },
  { path: 'materielPage', name: 'materiel-page', describe: '物料', group: "Materiel", show: '物料操作' },
  { path: 'bom', name: 'b-o-m', describe: 'BOM', group: "Materiel", show: 'BOM单接口' },
  { path: 'stockRoom', name: 'stockroom', describe: '仓库', group: "Materiel", show: '仓库物料信息' },
  { path: 'scan', name: 'scan', describe: '出入库扫描', group: "Materiel" },
  // {path:'TemporaryWarehouse',name: 'temporary-warehouse', describe:'临时仓库',group:"Materiel",show:'临时仓库物料信息'},
  // {path:'MaterielQuery',name: 'materiel-query', describe: '物料查询',group:"Materiel"},
  // {path:'MaterielCounting',name: 'materiel-counting', describe: '物料盘点',group:"Materiel"},
  // {path:'OverDue',name: 'over-due', describe: '过期预警',group:"Materiel",show:'物料过期预警信息'},
  // {path:'OutOfStock',name: 'out-of-stock', describe: '库存不足预警',group:"Materiel",show:'物料库存不足预警信息'},
  // {path:'AssignRemind',name: 'assign-remind', describe: '预警推送',group:"Materiel",show:'物料过期预警信息'},
  { path: 'Storage', name: 'storage', describe: '仓存', group: "Materiel" },
  { path: 'billexamine', name: 'bill-examine', describe: '单据审核', group: "Examine" },
  { path: 'bomexamine', name: 'bom-examine', describe: 'bom单审核', group: "Examine" },
  { path: 'planexamine', name: 'plan-examine', describe: '计划审核', group: "Examine" },
  // {path:'SystemPath',name: 'system-path', describe: '系统运维的一子tab',group:"Examine"}
];

//Vue.component('MaterielPage',()=>import('./views/materiel/component/materielPage/index.vue'))
Vue.prototype.$tabMenu.forEach(tab => Vue.component(tab.name, () => {
  return import ('./views/' + tab.group.toLowerCase() + '/component/' + tab.path + '/index.vue');
}));
Vue.prototype.$message = Message;
Vue.use(Loading.directive)
Vue.prototype.$confirm = MessageBox.confirm;

Vue.config.productionTip = false
Vue.use(permission)

if (process.env.BUILD_ENV == 'development') {
  Vue.config.devtools = true;
  require('../mock/index');
  console.log("mock open")
}

function VueRender() {
  new Vue({
    router,
    store,
    components: { Route },
    template: '<Route/>'
  }).$mount('#app')
}

VueRender()