/*
 * @Author: chiachan163
 * @Date: 2020/10/18 2:38 上午
 * @Last Modified by: chiachan163
 * @Last Modified time: 2020/10/18 2:38 上午
 */
const webpack = require('webpack');
const merge = require("webpack-merge");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpackConfigBase = require('./webpack.base.js');

const webpackConfigDev = {
  mode: "development",
  devtool: 'inline-source-map',
  devServer: {
    host: '127.0.0.1', // 主机地址
    port: 9000, // 端口号
    open: true,
    hot: true,
    overlay: {
      errors: true,
    },
    proxy: {
      "/api": {
        "target": "https://qlserver.tyu.wiki",
        // "target": "https://hfserver.g2mtu.cn",
        "changeOrigin": true,
        "pathRewrite": {"^/api": "/"}
      },
    },
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          "style-loader",
          "css-loader",
          "postcss-loader"
        ]
      },
      {
        test: /\.less$/,
        use: [
          "style-loader",
          "css-loader",
          "postcss-loader",
          "less-loader"
        ]
      },
    ],
  },
  stats: { children: false },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      chunksSortMode: 'none',
      filename: 'index.html',
      template: 'index.html',
      inject: true,
      hash: true,
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeAttributeQuotes: true
      }
    }),
  ]
}

module.exports = merge(webpackConfigBase, webpackConfigDev);
